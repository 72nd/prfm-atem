package osc

import (
	"fmt"
	"github.com/hypebeast/go-osc/osc"
	"gitlab.com/72nd/prfm-atem/pkg"
	"gitlab.com/72nd/prfm-atem/pkg/atem"
	"gitlab.com/72nd/prfm-atem/pkg/data"
	"regexp"
	"strconv"
)

type Server struct {
	Host        string
	Port        int
	MaxVidInput int
	Channels    data.Channels
	Mixer       *atem.Mixer
}

func NewServer(host string, port, maxVidInput int, chn data.Channels, mxr *atem.Mixer) *Server {
	return &Server{
		Host:        host,
		Port:        port,
		MaxVidInput: maxVidInput,
		Channels:    chn,
		Mixer:       mxr,
	}
}

func (s *Server) Serve() {
	d := osc.NewStandardDispatcher()
	if err := d.AddMsgHandler("*", s.handler); err != nil {
		pkg.Log.Error(err)
	}
	srv := &osc.Server{
		Addr:       fmt.Sprintf("%s:%d", s.Host, s.Port),
		Dispatcher: d,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			pkg.Log.Error(err)
		}
	}()
	pkg.Log.Infof("OSC server runs on %s with port %d", s.Host, s.Port)
}

func (s Server) handler(msg *osc.Message) {
	pkg.Log.Debug("got new OSC message with address ", msg.Address)
	if regexp.MustCompile(`^/(.*?)/(.*?)(/|)$`).MatchString(msg.Address) {
		s.argCmdHandler(msg.Address)
	} else {
		s.simpleCmdHandler(msg.Address)
	}
}

func (s Server) simpleCmdHandler(adr string) {
	results := regexp.MustCompile(`^/(.*?)(/|)$`).FindStringSubmatch(adr)
	if len(results) != 3 {
		pkg.Log.Errorf("could not parse «%s» as a simple cmd", adr)
		return
	}
	switch results[1] {
	case "run":
		pkg.Log.Warn("the run feature is not implemented yet")
	case "cut":
		s.Mixer.Cut()
	case "auto":
		s.Mixer.Auto()
	default:
		pkg.Log.Errorf("no action for command «%s» found", results[1])
	}
}

func (s Server) argCmdHandler(adr string) {
	results := regexp.MustCompile(`^/(.*?)/(.*?)/?$`).FindStringSubmatch(adr)
	if len(results) != 3 {
		pkg.Log.Error("could not parse osc address ", adr)
		return
	}

	arg, err := strconv.Atoi(results[2])
	if arg > s.MaxVidInput {
		pkg.Log.Errorf("the received chan number «%d», exceeds the ability of your device (max_video_input: %d)", arg, s.MaxVidInput)
		return
	}
	if err != nil {
		arg, err = s.Channels.GetChanNr(results[2])
		if err != nil {
			pkg.Log.Errorf("could not parse part «%s» in «%s» as chan number or handler", results[2], adr)
			return
		}
	}

	switch results[1] {
	case "preview":
		s.Mixer.PreviewById(arg)
	case "program":
		s.Mixer.ProgramById(arg)
	case "auto-set":
		s.Mixer.AutoSet(atem.Effect(arg))
	case "macro":
		s.Mixer.Macro(arg)
	default:
		pkg.Log.Error("could not found any command for ", adr)
	}
}
