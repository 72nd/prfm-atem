package util

import (
	"bufio"
	"fmt"
	"github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	"gitlab.com/72nd/prfm-atem/pkg/atem"
	"gitlab.com/72nd/prfm-atem/pkg/data"
	"gitlab.com/72nd/prfm-atem/pkg/osc"
	"gitlab.com/72nd/prfm-atem/pkg/webview"
	"os"
	"time"
)

type Runner struct{}

func (r *Runner) Run(path string, level logrus.Level) {
	logrus.SetLevel(level)
	logrus.SetFormatter(&prefixed.TextFormatter{})

	cfg := data.OpenConfig(path)
	mxr := atem.NewMixer(cfg.AtemHost, cfg.AtemCliExec)
	oscSrv := osc.NewServer(cfg.OscHost, cfg.OscPort, cfg.MaxVidInputs, cfg.Channels, mxr)
	go oscSrv.Serve()
	web := webview.NewServer(cfg, mxr)
	go web.Serve()
}

func PromptForExit() {
	reader := bufio.NewReader(os.Stdin)
	time.Sleep(200 * time.Millisecond)
	fmt.Println("Q + <Enter> to exit")

	for {
		input, _ := reader.ReadString('\n')
		if input == "Q\n" {
			os.Exit(0)
		} else {
			fmt.Println("Q + <Enter> to exit")
		}
	}
}
