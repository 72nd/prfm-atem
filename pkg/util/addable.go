package util

import (
	"fmt"
	"gitlab.com/72nd/prfm-atem/pkg"
	"gitlab.com/72nd/prfm-atem/pkg/atem"
	"gitlab.com/72nd/prfm-atem/pkg/data"
	ctrlData "gitlab.com/72nd/prfm-ctrl/pkg/data"
)

func GetAddable(path string) []ctrlData.Addable {
	cfg := data.OpenConfig(path)
	state, err := atem.ReadState(atem.NewMixer(cfg.AtemHost, cfg.AtemCliExec))
	if err != nil {
		pkg.Log.Error(err)
	}

	return []ctrlData.Addable{
		{
			Name:     "auto",
			Command:  fmt.Sprintf("/%s/auto", cfg.RouterPrefix),
			Children: nil,
		},
		{
			Name:     "auto-set",
			Command:  "",
			Children: []ctrlData.Addable{
				{
					Name:     "mix",
					Command:  fmt.Sprintf("/%s/auto-set/%d", cfg.RouterPrefix, atem.Mix),
					Children: nil,
				},
				{
					Name:     "dip",
					Command:  fmt.Sprintf("/%s/auto-set/%d", cfg.RouterPrefix, atem.Dip),
					Children: nil,
				},
				{
					Name:     "wipe",
					Command:  fmt.Sprintf("/%s/auto-set/%d", cfg.RouterPrefix, atem.Wipe),
					Children: nil,
				},
				{
					Name:     "sting",
					Command:  fmt.Sprintf("/%s/auto-set/%d", cfg.RouterPrefix, atem.Sting),
					Children: nil,
				},
				{
					Name:     "dve",
					Command:  fmt.Sprintf("/%s/auto-set/%d", cfg.RouterPrefix, atem.DVE),
					Children: nil,
				},
			},
		},
		{
			Name:     "cut",
			Command:  fmt.Sprintf("/%s/run", cfg.RouterPrefix),
			Children: nil,
		},
		{
			Name:     "macro",
			Command:  "",
			Children: macrosAddable(cfg.RouterPrefix, "macro", state, err),
		},
		{
			Name:     "preview",
			Command:  "",
			Children: inputsAddable(cfg.RouterPrefix, "preview", state, err),
		},
		{
			Name:     "program",
			Command:  "",
			Children: inputsAddable(cfg.RouterPrefix, "preview", state, err),
		},
		{
			Name:     "run",
			Command:  fmt.Sprintf("/%s/run", cfg.RouterPrefix),
			Children: nil,
		},
	}
}

func inputsAddable(prefix, cmd string, state *atem.State, stateError error) []ctrlData.Addable {
	if stateError != nil {
		return []ctrlData.Addable{}
	}
	adb := make([]ctrlData.Addable, len(state.VideoInputs))
	for i := range state.VideoInputs {
		adb[i] = ctrlData.Addable{
			Name:     state.VideoInputs[i].Name,
			Command:  fmt.Sprintf("/%s/%s/%d", prefix, cmd, state.VideoInputs[i].Index),
			Children: nil,
		}
	}
	return adb
}

func macrosAddable(prefix, cmd string, state *atem.State, stateError error) []ctrlData.Addable {
	if stateError != nil {
		return []ctrlData.Addable{}
	}
	adb := make([]ctrlData.Addable, len(state.Macros))
	for i := range state.Macros {
		adb[i] = ctrlData.Addable{
			Name:     state.Macros[i].Name,
			Command:  fmt.Sprintf("/%s/%s/%d", prefix, cmd, state.Macros[i].Index),
			Children: nil,
		}
	}
	return adb
}
