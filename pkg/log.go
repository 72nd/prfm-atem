package pkg

import "github.com/sirupsen/logrus"

var Log = logrus.WithField("prefix", "atem")
