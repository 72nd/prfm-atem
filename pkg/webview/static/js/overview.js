function oscPreview(prefix, index) {
    oscCopy(prefix, "preview/" + index);
}

function oscProgram(prefix, index) {
    oscCopy(prefix, "program/" + index);
}

function oscRunMacro(prefix, index) {
    oscCopy(prefix, "macro/" + index);
}

function oscCopy(prefix, cmd) {
    copy("/", prefix + "/" + cmd);
}
function copy(text) {
    alert(text);
    const el = document.createElement("textarea");
    el.value = text;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}