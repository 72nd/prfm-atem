package webview

import (
	"gitlab.com/72nd/prfm-atem/pkg"
	"gitlab.com/72nd/prfm-atem/pkg/atem"
	"gitlab.com/72nd/prfm-atem/pkg/data"
	"net/http"
)

type OverviewData struct {
	data.Config
	atem.State
}

func (s *Server) overviewHandler(w http.ResponseWriter, r *http.Request) {
	pkg.Log.Debug("overview page request")

	state, err := atem.ReadState(s.Mixer)
	if err != nil {
		s.errorHandler(w, r)
		return
	}

	viewData := OverviewData{
		Config: *s.Config,
		State:  *state,
	}
	if err := s.OverviewTemplate.Execute(w, viewData); err != nil {
		pkg.Log.Error("error while render overview template")
	}
}
