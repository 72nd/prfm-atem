package webview

import (
	"gitlab.com/72nd/prfm-atem/pkg"
	"net/http"
)

func (s *Server) errorHandler(w http.ResponseWriter, r *http.Request) {
	if err := s.ErrorTemplate.Execute(w, s.Config); err != nil {
		pkg.Log.Error("error while render overview template")
	}
}
