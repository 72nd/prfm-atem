package webview

import (
	"fmt"
	rice "github.com/GeertJohan/go.rice"
	"github.com/gorilla/mux"
	"gitlab.com/72nd/prfm-atem/pkg"
	"gitlab.com/72nd/prfm-atem/pkg/atem"
	"gitlab.com/72nd/prfm-atem/pkg/data"
	"html/template"
	"net/http"
)

type Server struct {
	Config           *data.Config
	Mixer            *atem.Mixer
	OverviewTemplate *template.Template
	ErrorTemplate    *template.Template
}

func NewServer(cfg *data.Config, mxr *atem.Mixer) *Server {
	return &Server{
		Config:           cfg,
		Mixer:            mxr,
		OverviewTemplate: getTemplate("static", "overview.html", "overview"),
		ErrorTemplate: getTemplate("static", "error.html", "error"),
	}
}

func getTemplate(folder, file, name string) *template.Template {
	tplBox, err := rice.FindBox(folder)
	if err != nil {
		pkg.Log.Fatal(err)
	}
	tplStr, err := tplBox.String(file)
	if err != nil {
		pkg.Log.Fatal(err)
	}
	tpl, err := template.New(name).Parse(tplStr)
	if err != nil {
		pkg.Log.Fatal(err)
	}
	return tpl
}

func (s *Server) Serve() {
	rtr := mux.NewRouter()
	rtr.HandleFunc("/", s.overviewHandler).Methods("GET")
	rtr.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(rice.MustFindBox("static").HTTPBox())))
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", s.Config.WebPort),
		Handler: rtr,
	}
	pkg.Log.Infof("start http server on port %d", s.Config.WebPort)
	if err := srv.ListenAndServe(); err != nil {
		pkg.Log.Fatal(err)
	}

}
