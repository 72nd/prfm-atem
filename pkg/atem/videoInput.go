package atem

import (
	"errors"
	"fmt"
	"gitlab.com/72nd/prfm-atem/pkg"
	"strconv"
	"strings"
)

type VideoExternalType int

const (
	VideoInternal VideoExternalType = iota
	VideoSDI
	VideoHDMI
	VideoComposite
	VideoComponent
	VideoSVideo
)

func (v VideoExternalType) String() string {
	switch v {
	case VideoInternal:
		return "Internal"
	case VideoSDI:
		return "SDI"
	case VideoHDMI:
		return "HDMI"
	case VideoComposite:
		return "Composite"
	case VideoComponent:
		return "Component"
	case VideoSVideo:
		return "SVideo"
	default:
		pkg.Log.Error("no name for given value found ", v)
		return fmt.Sprintf("ERR (%d)", int(v))
	}
}

type VideoInternalType int

const (
	VideoExternal VideoInternalType = iota
	VideoBlack
	VideoColorBars
	VideoColorGenerator
	VideoMediaPlayerFill
	VideoMediaPlayerKey
	VideoSuperSource
	VideoMeOutput  = 128
	VideoAuxiliary = 129
	VideoMask      = 130
)

func (v VideoInternalType) String() string {
	switch v {
	case VideoExternal:
		return "External"
	case VideoBlack:
		return "Black"
	case VideoColorBars:
		return "Color Bars"
	case VideoColorGenerator:
		return "Color Generator"
	case VideoMediaPlayerFill:
		return "Media Player Fill"
	case VideoMediaPlayerKey:
		return "Media Player Key"
	case VideoSuperSource:
		return "SuperSource"
	case VideoMeOutput:
		return "ME Output"
	case VideoAuxiliary:
		return "Auxiliary"
	case VideoMask:
		return "Mask"
	default:
		pkg.Log.Error("no name for given value found ", v)
		return fmt.Sprintf("ERR (%d)", int(v))
	}
}

type VideoInput struct {
	Index        int
	ExternalType VideoExternalType
	InternalType VideoInternalType
	Name         string
	IsPreview    bool
	IsProgram    bool
}

func NewVideoInputFromString(raw string) (*VideoInput, error) {
	parts := strings.Split(raw, ",")
	if len(parts) != 4 {
		return nil, errors.New(fmt.Sprintf("could not split «%s» into 4 parts", raw))
	}

	index, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[0]))
	}
	externalType, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[1]))
	}
	internalType, err := strconv.Atoi(parts[2])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[2]))
	}

	return &VideoInput{
		Index:        index,
		ExternalType: VideoExternalType(externalType),
		InternalType: VideoInternalType(internalType),
		Name:         parts[3],
		IsPreview:    false,
		IsProgram:    false,
	}, nil
}
