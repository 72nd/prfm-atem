package atem

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type Macro struct {
	Index int
	Used  bool
	Name  string
}

func NewMacroFromString(raw string) (*Macro, error) {
	parts := strings.Split(raw, ",")
	if len(parts) != 3 {
		return nil, errors.New(fmt.Sprintf("could not split «%s» into 3 parts", raw))
	}

	index, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[0]))
	}

	var used bool
	if parts[1] == "1" {
		used = true
	} else if parts[1] != "0" {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as bool", raw, parts[1]))
	}

	return  &Macro{
		Index: index,
		Used:  used,
		Name:  parts[2],
	}, nil
}