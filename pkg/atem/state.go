package atem

import (
	"errors"
	"fmt"
	"gitlab.com/72nd/prfm-atem/pkg"
)

type State struct {
	VideoInputs    []VideoInput
	AudioInputs    []AudioInput
	Macros         []Macro
	CurrentPreview *VideoInput
	CurrentProgram *VideoInput
}

func ReadState(mxr *Mixer) (*State, error) {
	vin, err := mxr.GetVideoInputs()
	if err != nil {
		return nil, err
	}
	ain, err := mxr.GetAudioInputs()
	if err != nil {
		return nil, err
	}
	mcr, err := mxr.GetMacros()
	if err != nil {
		return nil, err
	}
	status, err := mxr.GetStatus()
	if err != nil {
		return nil, err
	}

	state := State{
		VideoInputs: vin,
		AudioInputs: ain,
		Macros:      mcr,
	}
	preview, err := state.VideoInputByIndex(status.PreviewIndex)
	if err != nil {
		pkg.Log.Error("could not find preview video-input: ", err)
	}
	preview.IsPreview = true
	state.CurrentPreview = preview

	program, err := state.VideoInputByIndex(status.ProgramIndex)
	if err != nil {
		pkg.Log.Error("could not find program video-input: ", err)
	}
	program.IsProgram = true
	state.CurrentProgram = program

	return &state, nil
}

func (s State) VideoInputByIndex(index int) (*VideoInput, error) {
	for i := range s.VideoInputs {
		if s.VideoInputs[i].Index == index {
			return &s.VideoInputs[i], nil
		}
	}
	return nil, errors.New(fmt.Sprintf("no video input for given index «%d» found", index))
}
