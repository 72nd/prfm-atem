package atem

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"gitlab.com/72nd/prfm-atem/pkg"
	"os/exec"
	"strconv"
	"strings"
)

type Command string

const (
	CmdRead        Command = "read"
	CmdProgram             = "program"
	CmdPreview             = "preview"
	CmdCut                 = "cut"
	CmdAuto                = "auto"
	CmdAutoRead            = "auto-read"
	CmdAutoSet             = "auto-set"
	CmdMacro               = "macro"
	CmdStatus              = "status"
	CmdListMarcos          = "macros"
	CmdVideoInputs         = "video-inputs"
	CmdAudioInputs         = "audio-inputs"
)

type Effect int

const (
	Mix Effect = iota
	Dip
	Wipe
	Sting
	DVE
)

func (m Mixer) Read() {
	pkg.Log.Debug("exec read command")
	go func() {
		_, _ = m.execCliCmd(CmdRead, -1)
	}()
}

func (m Mixer) ProgramById(channel int) {
	pkg.Log.Debug("exec program command with arg ", channel)
	go func() {
		_, _ = m.execCliCmd(CmdProgram, channel)
	}()
}

func (m Mixer) PreviewById(channel int) {
	pkg.Log.Debug("exec preview command with arg ", channel)
	go func() {
		_, _ = m.execCliCmd(CmdPreview, channel)
	}()
}

func (m Mixer) Cut() {
	pkg.Log.Debug("exec cut command")
	go func() {
		_, _ = m.execCliCmd(CmdCut, -1)
	}()
}

func (m Mixer) Auto() {
	pkg.Log.Debug("exec auto command")
	go func() {
		_, _ = m.execCliCmd(CmdAuto, -1)
	}()
}

func (m Mixer) AutoRead() {
	pkg.Log.Debug("exec auto-read command")
	go func() {
		_, _ = m.execCliCmd(CmdAutoRead, -1)
	}()
}

func (m Mixer) AutoSet(effect Effect) {
	pkg.Log.Debug("exec aut-set command with arg ", effect)
	go func() {
		_, _ = m.execCliCmd(CmdAutoSet, int(effect))
	}()
}

func (m Mixer) Macro(i int) {
	pkg.Log.Debug("exec macro command with arg ", i)
	go func() {
		_, _ = m.execCliCmd(CmdMacro, i)
	}()
}

func (m Mixer) GetStatus() (*Status, error) {
	pkg.Log.Debug("exec status command")
	lines, err := m.execCliCmd(CmdStatus, -1)
	if err != nil {
		return nil, err
	}
	status, err := NewStatusFromString(lines[0])
	if err != nil {
		pkg.Log.Error("could not parse status: ", err)
	}
	return status, nil
}

func (m Mixer) GetMacros() ([]Macro, error) {
	pkg.Log.Debug("exec macros command")
	lines, err := m.execCliCmd(CmdListMarcos, -1)
	if err != nil {
		return nil, err
	}
	var macros = make([]Macro, notEmptyLen(lines))
	for i := range lines {
		if lines[i] == "" {
			pkg.Log.Debug("skip empty line")
			continue
		}
		macro, err := NewMacroFromString(lines[i])
		if err != nil {
			pkg.Log.Error("could not parse macro: ", err)
			continue
		}
		macros[i] = *macro
	}
	return macros, nil
}

func (m Mixer) GetVideoInputs() ([]VideoInput, error) {
	pkg.Log.Debug("exec video-inputs command")
	lines, err := m.execCliCmd(CmdVideoInputs, -1)
	if err != nil {
		return nil, err
	}
	var inputs = make([]VideoInput, notEmptyLen(lines))
	for i := range lines {
		if lines[i] == "" {
			pkg.Log.Debug("skip empty line")
			continue
		}
		input, err := NewVideoInputFromString(lines[i])
		if err != nil {
			pkg.Log.Error("could not parse video-input: ", err)
			continue
		}
		inputs[i] = *input
	}
	return inputs, nil
}

func (m Mixer) GetAudioInputs() ([]AudioInput, error) {
	pkg.Log.Debug("exec audio-inputs command")
	lines, err := m.execCliCmd(CmdAudioInputs, -1)
	if err != nil {
		return nil, err
	}
	var inputs = make([]AudioInput, notEmptyLen(lines))
	for i := range lines {
		if lines[i] == "" {
			pkg.Log.Debug("skip empty line")
			continue
		}
		input, err := NewAudioInputFromString(lines[i])
		if err != nil {
			pkg.Log.Error("could not parse audio-input: ", err)
			continue
		}
		inputs[i] = *input
	}
	return inputs, nil
}

// Call atem_cli with a command and an argument, if there is no argument use -1
func (m Mixer) execCliCmd(cmd Command, arg int) ([]string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), m.timeout)
	defer cancel()

	var args []string
	if arg == -1 {
		args = []string{m.atemHost, string(cmd)}
	} else {
		args = []string{m.atemHost, string(cmd), strconv.Itoa(arg)}
	}

	var errBuffer, stdBuffer bytes.Buffer
	exe := exec.CommandContext(ctx, m.atemCliExec, args...)
	exe.Stderr = &errBuffer
	exe.Stdout = &stdBuffer
	if err := exe.Run(); err != nil {
		return []string{}, fmt.Errorf("error while execute atem_cli with cmd «%s» and arg «%d»: %s", cmd, arg, err)
	}
	if strings.Contains(stdBuffer.String(), "connection timeout!") {
		return []string{}, errors.New("atem could not be reached, or too many commands were sent in a short period of time")
	}
	return extractData(stdBuffer), nil
}

func extractData(buf bytes.Buffer) []string {
	parts := strings.Split(buf.String(), "\n---\n")
	if len(parts) != 2 {
		pkg.Log.Error("could not split output into two parts with «---»: ", buf.String())
		return nil
	}
	return strings.Split(parts[1], "\n")
}

func notEmptyLen(slice []string) int {
	l := 0
	for i := range slice {
		if slice[i] != "" {
			l++
		}
	}
	return l
}
