package atem

import (
	"errors"
	"fmt"
	"gitlab.com/72nd/prfm-atem/pkg"
	"strconv"
	"strings"
)

type AudioType int

const (
	AudioVideoInput AudioType = iota
	AudioMediaPlayer
	AudioExternal
)

func (a AudioType) String() string {
	switch a {
	case AudioVideoInput:
		return "Video input"
	case AudioMediaPlayer:
		return "Media player"
	case AudioExternal:
		return "External"
	default:
		pkg.Log.Error("no name for given value found ", a)
		return fmt.Sprintf("ERR (%d)", int(a))
	}
}

type AudioPlugType int

const (
	AudioInternal AudioPlugType = iota
	AudioSDI
	AudioComponent
	AudioComposite
	AudioSVideo
	AudioXLR    = 32
	AudioAesEbu = 64
	AudioRCA    = 128
)

func (a AudioPlugType) String() string {
	switch a {
	case AudioInternal:
		return "Internal"
	case AudioSDI:
		return "SDI"
	case AudioComponent:
		return "Component"
	case AudioComposite:
		return "Composite"
	case AudioSVideo:
		return "SVideo"
	case AudioXLR:
		return "XLR"
	case AudioAesEbu:
		return "AES/EBU"
	case AudioRCA:
		return "RCA"
	default:
		pkg.Log.Error("no name for given value found ", a)
		return fmt.Sprintf("ERR (%d)", int(a))
	}
}

type AudioState int

const (
	AudioOff AudioState = iota
	AudioOn
	AudioAFV
)

func (a AudioState) String() string {
	switch a {
	case AudioOff:
		return "Off"
	case AudioOn:
		return "On"
	case AudioAFV:
		return "AFV"
	default:
		pkg.Log.Error("no name for given value found ", a)
		return fmt.Sprintf("ERR (%d)", int(a))
	}
}

type AudioInput struct {
	Index    int
	Type     AudioType
	PlugType AudioPlugType
	State    AudioState
	Balance  float64
	Gain     float64
}

func NewAudioInputFromString(raw string) (*AudioInput, error) {
	parts := strings.Split(raw, ",")
	if len(parts) != 6 {
		return nil, errors.New(fmt.Sprintf("could not split «%s» into 6 parts", raw))
	}

	index, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[0]))
	}
	audioType, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[1]))
	}
	plugType, err := strconv.Atoi(parts[2])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[2]))
	}
	state, err := strconv.Atoi(parts[3])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[3]))
	}
	balance, err := strconv.ParseFloat(parts[4], 64)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as float", raw, parts[4]))
	}
	gain, err := strconv.ParseFloat(parts[5], 64)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as float", raw, parts[5]))
	}

	return &AudioInput{
		Index:    index,
		Type:     AudioType(audioType),
		PlugType: AudioPlugType(plugType),
		State:    AudioState(state),
		Balance:  balance,
		Gain:     gain,
	}, nil
}
