package atem

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type Status struct {
	PreviewIndex int
	ProgramIndex int
}

func NewStatusFromString(raw string) (*Status, error) {
	parts := strings.Split(raw, ",")
	if len(parts) != 2 {
		return nil, errors.New(fmt.Sprintf("could not split «%s» into 2 parts", raw))
	}

	preview, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[0]))
	}
	program, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not parse «%s» from «%s» as int", raw, parts[1]))
	}

	return &Status{
		PreviewIndex: preview,
		ProgramIndex: program,
	}, nil
}
