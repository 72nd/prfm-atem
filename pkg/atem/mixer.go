package atem

import (
	"time"
)

type Mixer struct {
	atemHost            string
	atemCliExec         string
	timeout             time.Duration
}

func NewMixer(atemHost string, exec string) *Mixer {
	mxr := &Mixer{
		atemHost:    atemHost,
		atemCliExec: exec,
		timeout:     3 * time.Second,
	}
	return mxr
}
