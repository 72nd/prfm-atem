package data

import (
	"fmt"
	"github.com/creasty/defaults"
	"gitlab.com/72nd/prfm-atem/pkg"
)

type Channels struct {
	Chan0 string `json:"chan_0" default:"black"`
	Chan1 string `json:"chan_1" default:"hdmi1"`
	Chan2 string `json:"chan_2" default:"hdmi2"`
	Chan3 string `json:"chan_3" default:"hdmi3"`
	Chan4 string `json:"chan_4" default:"hdmi4"`
	Chan5 string `json:"chan_5" default:"hdmi4"`
	Chan6 string `json:"chan_6" default:"sdi1"`
	Chan7 string `json:"chan_7" default:"sdi2"`
	Chan8 string `json:"chan_8" default:"sdi3"`
	Chan9 string `json:"chan_9" default:"sdi4"`
}

func NewChannels() Channels {
	var cid Channels
	if err := defaults.Set(&cid); err != nil {
		pkg.Log.Error("default config generation failed: ", err)
	}
	return cid
}

func (c Channels) GetChanNr(handler string) (int, error) {
	switch handler {
	case c.Chan0:
		return 0, nil
	case c.Chan1:
		return 1, nil
	case c.Chan2:
		return 2, nil
	case c.Chan3:
		return 3, nil
	case c.Chan4:
		return 4, nil
	case c.Chan5:
		return 5, nil
	case c.Chan6:
		return 6, nil
	case c.Chan7:
		return 7, nil
	case c.Chan8:
		return 8, nil
	case c.Chan9:
		return 9, nil
	default:
		return -1, fmt.Errorf("no channel number for handler «%s» found", handler)
	}
}

