package data

import (
	"encoding/json"
	"github.com/creasty/defaults"
	"gitlab.com/72nd/prfm-atem/pkg"
	"io/ioutil"
)

type Config struct {
	OscHost      string   `json:"osc_host" default:"127.0.0.1"`
	OscPort      int      `json:"osc_port" default:"9002"`
	WebPort      int      `json:"web_port" default:"8002"`
	AtemHost     string   `json:"atem_host" default:"192.168.1.240"`
	AtemCliExec  string   `json:"atem_cli_exec" default:"atem-cli"`
	RouterPrefix string   `json:"router_prefix" default:""`
	Channels     Channels `json:"channel_handlers" default:""`
	MaxVidInputs int      `json:"max_vid_inputs" default:"8"`
	path         string   `json:"-"`
}

func NewConfig(path string) *Config {
	cfg := &Config{
		path: path,
	}
	if err := defaults.Set(cfg); err != nil {
		pkg.Log.Error("default config generation failed: ", err)
	}
	cfg.Channels = NewChannels()
	return cfg
}

func OpenConfig(path string) *Config {
	cfg := &Config{
		path: path,
	}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		pkg.Log.Fatalf("error while reading %s from disk: %s", path, err.Error())
	}
	if err := json.Unmarshal(data, cfg); err != nil {
		pkg.Log.Fatal("error while parsing config json: ", err)
	}
	pkg.Log.Debugf("config loaded from %s", path)
	return cfg
}

func (c *Config) SaveConfig() {
	data, err := json.Marshal(c)
	if err != nil {
		pkg.Log.Error("error while JSON marshal: ", err)
	}
	if err := ioutil.WriteFile(c.path, data, 0644); err != nil {
		pkg.Log.Error("error while write JSON to disk: ", err)
	}
}
