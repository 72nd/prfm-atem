#!/bin/sh

# libqatemcontrol
sudo apt install qt4-qmake libqt4-dev
git clone https://github.com/petersimonsson/libqatemcontrol.git
cd libqatemcontrol
qmake
make
sudo make install
cd ..
rm -rf libqatemcontrol

# atem_cli
git clone https://github.com/72nd/atem_cli.git
cd atem_cli
rm moc_App.cpp
qmake
make
cp atem_cli ../atem-cli
cd ..
rm -rf atem_cli
