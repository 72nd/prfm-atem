package main

import (
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/72nd/prfm-atem/pkg"
	"gitlab.com/72nd/prfm-atem/pkg/data"
	"gitlab.com/72nd/prfm-atem/pkg/util"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "prfm-atem"
	app.Usage = "control a ATEM video switcher via a web interface or osc"
	app.Action = func(c *cli.Context) error {
		_ = cli.ShowCommandHelp(c, c.Command.Name)
		return nil
	}
	app.Commands = []cli.Command{
		{
			Name:  "new",
			Usage: "create a new default config file",
			Action: func(c *cli.Context) error {
				if len(c.Args()) != 1 {
					pkg.Log.Error("no path to save config provided")
					return nil
				}
				cfg := data.NewConfig(c.Args().First())
				cfg.SaveConfig()
				return nil
			},
		},
		{
			Name:    "run",
			Aliases: []string{"serve"},
			Usage:   "run the http and osc server",
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:  "debug, d",
					Usage: "enable the debug mode",
				},
				cli.BoolFlag{
					Name:  "trace, t",
					Usage: "enable debug mode with detailed output",
				},
				cli.BoolFlag{
					Name:  "atem-debug, a",
					Usage: "enable the debug mode of the atem client",
				},
			},
			Action: func(c *cli.Context) error {
				if len(c.Args()) != 1 {
					pkg.Log.Error("no config file provided")
					return nil
				}
				level := logrus.InfoLevel
				if c.Bool("debug") {
					level = logrus.DebugLevel
				}
				if c.Bool("trace") {
					level = logrus.TraceLevel
				}
				runner := util.Runner{}
				runner.Run(c.Args().First(), level)
				util.PromptForExit()
				return nil
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		pkg.Log.Error(err)
	}
}
