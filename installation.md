# Installation of atem cli

## libqatemcontrol

Install dependencies

```
sudo apt install qt4-qmake libqt4-dev
```

Clone, compile and install

```
git clone https://github.com/petersimonsson/libqatemcontrol.git
cd libqatemcontrol
qmake
make
sudo make install
```

## atem_cli

```
git clone https://github.com/72nd/atem_cli.git
cd atem_cli
rm moc_App.cpp
qmake
make
