module gitlab.com/72nd/prfm-atem

go 1.13

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/creasty/defaults v1.3.0
	github.com/gorilla/mux v1.7.3
	github.com/hypebeast/go-osc v0.0.0-20200115085105-85fee7fed692
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/onsi/ginkgo v1.11.0 // indirect
	github.com/onsi/gomega v1.8.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli v1.22.2
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad // indirect
)
